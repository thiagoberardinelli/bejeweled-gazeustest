using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public Vector2Int TileIndex;

    public bool alreadyMatched;
    public Color SelectedColor;

    [Space(2.5f)]
    [Header("Visual Effects")]
    public ParticleSystem OnMatchParticleSystem;    

    [SerializeField] private TileData _tileData;
    public TileData TileData
    {
        get
        {
            return _tileData;
        }
        set
        {
            _tileData = value;

            if (SpriteRenderer != null)
            {
                bool isNotNull = value != null;
                SpriteRenderer.enabled = isNotNull;

                if (isNotNull)
                {
                    SpriteRenderer.sprite = value.TileSprite;
                }
            }
        }
    }

    private SpriteRenderer SpriteRenderer;

    private void Awake()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();  
    }

    // Updates tile position in the boardgame grid
    public void UpdatePositionInGrid(int x, int y)
    {
        TileIndex = new Vector2Int(x, y);
        // For debug proporses 
        gameObject.name = TileIndex.x + "," + TileIndex.y;
    }

    private void OnMouseDown()
    {
        // If you cant interact with the board, stops the function
        if (!BoardController.Instance.CanSwapTile) return;

        if (BoardController.Instance.CurrentSelectedTile != null)
        {
            // If the tile selected is the current selected tile in the board manager, stops the function
            if (BoardController.Instance.CurrentSelectedTile == this) return;

            // Deselect the old value of current selected tile in the board manager
            BoardController.Instance.CurrentSelectedTile.DeselectTile();

            // Checks the distance between the two interacted tiles
            var distance = Vector2Int.Distance(BoardController.Instance.CurrentSelectedTile.TileIndex, TileIndex);

            // if the distance is equals to one (meaning the tiles are side by side) swap the two interacted tiles, than set the current selected tile in the board manager as null
            if (distance == 1)
            {
                StartCoroutine(BoardController.Instance.SwapTiles(this));
                BoardController.Instance.CurrentSelectedTile = null;
            }
            // if its bigger than 1(distance between the two tile centers), set the current selected tile in the board manager as the new tile clicked
            else
            {
                BoardController.Instance.CurrentSelectedTile = this;
                SelectTile();
            }
        }
        // If there is no tile selected as current tile in the board manager, set this tile as current
        else
        {
            BoardController.Instance.CurrentSelectedTile = this;
            SelectTile();
        }
    }

    // Change tile color when selected
    public void SelectTile()
    {
        SpriteRenderer.color = SelectedColor;
    }

    // Swap back to tile original color when deselected
    public void DeselectTile()
    {
        SpriteRenderer.color = Color.white;
    }

    public void OnMatch()
    {
        // Particle Effects  
        ParticleSystem.MainModule mainModule = OnMatchParticleSystem.main;
        // Apply the correspondent tile data color to the particle effect
        mainModule.startColor = TileData.OnMatchParticleSystemColor;
        // Start the particle system
        OnMatchParticleSystem.Play();
        // Add this tile score to the score manager
        ScoreController.instance.UpdateScoreText((int)TileData.Points);
        // Set all tile data to null
        TileData = null;
    }
}
