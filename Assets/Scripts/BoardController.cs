using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

public class BoardController : MonoBehaviour
{
    public static BoardController Instance { get; private set; }
    public Tile CurrentSelectedTile = null; // Value of the current selected tile

    public Tile TilePrefab; // Tile Prefab
    public List<TileData> TilesData; // TileData 

    [HideInInspector] public bool CanSwapTile = true; // control bool for board any interaction

    [Space(2.5f)]
    [Header("Grid Size")]
    [SerializeField] private int _rowCount; // number of rows
    [SerializeField] private int _columnCount; // numer of columns

    private Tile[,] _tilesGrid; // matrix of tiles (gameboard)
    private Vector2 _tileOffset; // offset between tiles
    private bool _isACombination; // vari�vel boolena que diz que pelo menos uma combina��o de 3 tiles foi encontrada

    [SerializeField] private float _tileVFXdelay; // delay between every tile match-3 visual effect
    [HideInInspector] public bool IsInAMatchEffect; // bool that checks if any visual effect is happening 

    public event Action<int> TilesMove; // Move numbers event
    public event Action BoardReset; // Board reset event

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Start()
    {
        // get each tile bounds size
        _tileOffset = TilePrefab.GetComponent<SpriteRenderer>().bounds.size;

        // Generate the game board
        GenerateGameBoard();
    }

    private void GenerateGameBoard()
    {
        _tilesGrid = new Tile[_rowCount, _columnCount];

        TileData[] tileDataOnLeft = new TileData[_columnCount];
        TileData tileDataOnBottom = null;

        for (int i = 0; i < _rowCount; i++)
        {
            for (int j = 0; j < _columnCount; j++)
            {
                // Tile instanciation in each matrix possible position                
                Tile tile = Instantiate(TilePrefab, new Vector3(transform.position.x + (_tileOffset.x * i), transform.position.y + (_tileOffset.y * j), 0f),
                    Quaternion.identity, transform);

                // Updates the tile position in the matrix in Tile class
                tile.UpdatePositionInGrid(i, j);

                // List of all possibilities to avoid a 3 or bigger match in the first board generation                
                List<TileData> _possibleTileData = new List<TileData>();
                _possibleTileData.AddRange(TilesData);

                // Remove the left tileData information from the list
                _possibleTileData.Remove(tileDataOnLeft[j]);
                // Remove bottom tileData information from the list
                _possibleTileData.Remove(tileDataOnBottom);

                // Random a value of the new TileDatas possibilities and than set it
                TileData tileData = _possibleTileData[UnityEngine.Random.Range(0, _possibleTileData.Count)];
                tile.TileData = tileData;

                // Set the values of adjancent tiles to the next loop iteration               
                tileDataOnLeft[j] = tileData;
                tileDataOnBottom = tileData;

                // Define the tile in the correspondent position in the matrix for this iteration
                _tilesGrid[i, j] = tile;
            }
        }
    }

    public IEnumerator SwapTiles(Tile secondTile)
    {
        // Plays the swap in SoundEffect
        AudioManager.instance.PlaySoundOneShot("SFX_TileSwapIn");
        // Add one movement to the move couter int the HUD
        TilesMove(1);

        TileData firstTileData = CurrentSelectedTile.TileData;
        // blocks a new tile interaction in any place on the board
        CanSwapTile = false;

        // swap the TileData of the tiles of the two tiles interacted
        CurrentSelectedTile.TileData = secondTile.TileData;
        secondTile.TileData = firstTileData;        

        if (!CheckPossibleCombinations())
        {
            // If there is no match-3 (or bigger) in the hole board, return the two tiles to it's original positions and stops the current courotine and calls the swapback one        
            StartCoroutine(SwapTilesBack(secondTile, firstTileData));
            yield break;
        }
        // If there is any match-3 combination
        else
        {
            // Variables for pitch effect purposes 
            float soundPitch = 1f;
            float pitchMultiplier = 1f;
            do
            {
                // multiplier the current pitch for a sharper sound effect
                soundPitch *= pitchMultiplier;
                pitchMultiplier += 0.075f;
                // plays the match sound effect with its respective pitch value
                AudioManager.instance.PlaySoundOneShot("SFX_Match", soundPitch);
                // If there is any match-3 effect running, this should wait it ends to beggin filling the holes
                yield return new WaitUntil(() => IsInAMatchEffect == false);
                // Calls the refill empty tiles function and waits for the end of it before starting the next while iteration (if there is any)
                yield return StartCoroutine(FillEmptySlots());          
            }
            while (CheckPossibleCombinations());

            if (pitchMultiplier != 1)
                pitchMultiplier = 1;
        }


        // Enables back the interaction with any tile in the board
        CanSwapTile = true;
    }

    // Courotine responsible for swaping the two interacted tiles
    private IEnumerator SwapTilesBack(Tile secondTile, TileData firstTileData)
    {
        Tile tile = CurrentSelectedTile;

        // Delay to swipe the tiles back
        yield return new WaitForSeconds(0.25f);
        // Plays the return to previous position SFX
        AudioManager.instance.PlaySoundOneShot("SFX_TileSwapBack");

        // Revert the TileData information to the original values in the beggining of the swap
        secondTile.TileData = tile.TileData;
        tile.TileData = firstTileData;

        // Unlock a new tile interaction
        CanSwapTile = true;
    }

    // Checks the board for possible combinations 
    private bool CheckPossibleCombinations()
    {
        HashSet<Tile> combinedTiles = new HashSet<Tile>();

        for (int i = 0; i < _rowCount; i++)
        {
            for (int j = 0; j < _columnCount; j++)
            {
                Tile currentTileData = GetTileData(i, j);

                // List of tiles in case o a horizontal match
                List<Tile> horizontalMatches = SearchMatchesInColumn(i, j, currentTileData);
                // if the list count is bigger than 2, proceed
                if (horizontalMatches.Count >= 2)
                {
                    // Adds the current tile to meet the requirements of at least 3 tiles in a sequence
                    horizontalMatches.Add(currentTileData);
                    // Adds the list to the hashset of tiles
                    combinedTiles.UnionWith(horizontalMatches);
                }

                // // List of tiles in case o a vertical match
                List<Tile> verticalMatches = SearchMatchesInRow(i, j, currentTileData);

                // if the list count is bigger than 2, proceed
                if (verticalMatches.Count >= 2)
                {
                    // Adds the current tile to meet the requirements of at least 3 tiles in a sequence
                    verticalMatches.Add(currentTileData);
                    // Combine the list to the current hashset value
                    combinedTiles.UnionWith(verticalMatches);
                }
            }
        }

        // Coroutine that controls the match-3 effect in all combinations found
        StartCoroutine(MatchTilesEffect(combinedTiles));

        // Returns true if the hashset instance is bigger than 0 (meaning that there is at least one combination). False if not
        return combinedTiles.Count > 0;
    }

    private IEnumerator MatchTilesEffect(HashSet<Tile> combinedTiles)
    {
        // Set the control bool of tile effects to true
        IsInAMatchEffect = true;

        foreach (Tile tile in combinedTiles)
        {            
            // Fuction that does the particle effects
            tile.OnMatch();
            // Delay between tiles to create a cascade effect 
            yield return new WaitForSeconds(_tileVFXdelay);
        }

        // Set it back to false
        IsInAMatchEffect = false;
    }

    private Tile GetTileData(int gridRow, int gridColumn)
    {
        // Checks if is matrix range
        if (gridRow < 0 || gridRow >= _rowCount) return null;
        if (gridColumn < 0 || gridColumn > _columnCount) return null;

        Tile currentTileData = _tilesGrid[gridRow, gridColumn];

        // returns the tile in a givem row and column
        return currentTileData;
    }

    // List that checks the tileData of adjacent tiles in the same line    
    private List<Tile> SearchMatchesInRow(int gridRow, int gridColumn, Tile currentTile)
    {
        List<Tile> horizontalMatches = new List<Tile>();
        
        for (int i = gridRow + 1; i < _rowCount; i++)
        {
            // Checks the adjacent tile on the right
            Tile nextRowTileData = GetTileData(i, gridColumn);

            // Comparte the current tile ID in the iteration to the original tile
            if (nextRowTileData.TileData.ID != currentTile.TileData.ID) break;

            // If true, add it to the horizontalMatches list
            horizontalMatches.Add(nextRowTileData);
        }      

        return horizontalMatches;
    }

    // List that checks the tileData of adjacent tiles in the same column    
    private List<Tile> SearchMatchesInColumn(int gridRow, int gridColumn, Tile currentTileSprite)
    {
        List<Tile> verticalMatches = new List<Tile>();

        for (int i = gridColumn + 1; i < _columnCount; i++)
        {
            // Checks the vertical adjacent tile 
            Tile nextColumnTileData = GetTileData(gridRow, i);

            // Comparte the current tile ID in the iteration to the original tile
            if (nextColumnTileData.TileData.ID != currentTileSprite.TileData.ID) break;

            // If true, add it to the verticalMatches list
            verticalMatches.Add(nextColumnTileData);
        }

        return verticalMatches;
    }

    // Coroutine responsible for filling the holes in the board after one or more matches
    private IEnumerator FillEmptySlots()
    {        
        // iterates through the board tiles
        for (int i = 0; i < _columnCount; i++)
        {
            for (int j = 0; j < _rowCount; j++) 
            {
                // while the current check tile is null
                while (GetTileData(i, j).TileData == null) 
                {
                    // check for all tiles above and set it tilaData to the tile bellow
                    for (int filler = j; filler < _rowCount - 1; filler++) 
                    {
                        Tile current = GetTileData(i, filler);
                        Tile next = GetTileData(i, filler + 1);
                        current.TileData = next.TileData;                        
                    }
                    //  Gets the last vertical tile int the column
                    Tile last = GetTileData(i, _columnCount - 1);
                    // Set the last tile of the column to a random value
                    last.TileData = TilesData[UnityEngine.Random.Range(0, TilesData.Count)];
                    // waits 0.1 seconds to the next interaction
                    yield return new WaitForSeconds(0.1f);
                }                
            }            
        }
    }

    // Prepare the board for a new game
    public void ResetBoard()
    {
        if (!CanSwapTile) return;

        // Set all current tileData to null
        for (int i = 0; i < _rowCount; i++)
        {
            for (int j = 0; j < _columnCount; j++)
            {               
                _tilesGrid[i, j].TileData = null;
            }
        }

        GenerateGameBoard(); // Generates a new board
        BoardReset(); // Raise the boardreset event
    }
}