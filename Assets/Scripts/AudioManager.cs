using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    // Array containing all desireble sounds
    public Sound[] sounds; 

    public static AudioManager instance;

    void Awake()
    {        
        if (instance == null)
        {
            instance = this;           
        }

        AudioManagerSetup();
    }
        
    // Setup for the audio manager
    private void AudioManagerSetup()
    {
        // For each sound in the array adds a AudioSourceComponent
        foreach (Sound sound in instance.sounds)
        {            
            sound.source = gameObject.AddComponent<AudioSource>();

            // Set all the variables for this current audioSource
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }
    }

    // Getter of the desired audio source
    public AudioSource GetAudioSource(string name)
    {        
        // Find the correspondent audio source in the array of sounds by checking its name
        Sound currentSound = Array.Find(instance.sounds, sound => sound.name == name);

        // If there is no match, return null
        if (currentSound == null) return null;
      
        return currentSound.source;
    }

    // Plays one shot
    public void PlaySoundOneShot(string name)
    {
        AudioSource audioSource = GetAudioSource(name);    
        audioSource.PlayOneShot(audioSource.clip);
    }

    // Overload with pitch manipulation
    public void PlaySoundOneShot(string name, float pitch)
    {
        AudioSource audioSource = GetAudioSource(name);
        audioSource.pitch = pitch;
        audioSource.PlayOneShot(audioSource.clip);
    }

    // Plays the audio
    public void PlaySound(string name)
    {
        GetAudioSource(name).Play();
    }

    // Stop a current sound playing
    public void StopSound(string name)
    {
        // if the current audio is not playing than return
        if (!GetAudioSource(name).isPlaying) return;

        GetAudioSource(name).Stop();
    }

    // Mute all sounds of a given type (SFX or Music)
    public void MuteSoundsByType(Sound.SoundType type)
    {
        foreach (Sound sound in instance.sounds)
        {
            if (sound.soundType == type)
            {
                sound.source.mute = !sound.source.mute;                
            }
        }

    }
}

[Serializable]
public class Sound
{
    public string name; // Audio name -> the ideia here is to play the desired sound by its name

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume = 0.5f; // Volume control
    [Range(0.1f, 3f)]
    public float pitch = 1f; // Pitch Controle

    public bool loop;

    public enum SoundType { Music, SFX } // Enumerator that defines the type of sound (Sound Effect or music)
    public SoundType soundType;

    [HideInInspector]
    public AudioSource source; // Audio source for each audio clip

}
