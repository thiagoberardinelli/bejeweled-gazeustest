using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tile", menuName = "Tiles/TileData")]
public class TileData : ScriptableObject
{
    public int ID; // Tile ID
    public int Points; // Points for matching this tile
    public Sprite TileSprite; // Tile Sprite
    public Color OnMatchParticleSystemColor; // Tile particle effect (VFX) color
}
