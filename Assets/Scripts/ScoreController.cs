using TMPro;
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreController : MonoBehaviour
{
    public static ScoreController instance;

    public TextMeshProUGUI ScoreText; // score hud text
    public TextMeshProUGUI MovesText; // moves count hud text

    private int _currentScore = 0; // current score
    private int _lastScoreValue; // old score value

    private int _currentMoves = 0; // current number of moves

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {       
        // Register the events
        BoardController.Instance.TilesMove += UpdateMoveCountText;
        BoardController.Instance.BoardReset += ResetAllValues;
    }

    private void OnDestroy()
    {        
        // Unregister on destroy
        BoardController.Instance.TilesMove -= UpdateMoveCountText;
        BoardController.Instance.BoardReset -= ResetAllValues;
    }

    public void UpdateScoreText(int score)
    {
        _lastScoreValue = _currentScore;
        // Adds the new score to the currentScore value
        _currentScore += score;
        // Calls the score increment effect coroutine
        StartCoroutine(ScoreIncrement());        
    }

    private void UpdateMoveCountText(int move)
    {
        // Add one to move count
        _currentMoves++;
        MovesText.text = _currentMoves.ToString();
    }

    // Score unitary increment effect
    private IEnumerator ScoreIncrement()
    {       
        while (_lastScoreValue < _currentScore)
        {
            _lastScoreValue++;
            ScoreText.text = _lastScoreValue.ToString();
            yield return new WaitForSeconds(0.05f);
        }

        _lastScoreValue = _currentScore;
    }

    // Reset all variable values to default 
    private void ResetAllValues()
    {
        _currentScore = 0;
        _lastScoreValue = 0;
        _currentMoves = 0;

        MovesText.text = _currentMoves.ToString();
        ScoreText.text = _currentScore.ToString();
    }
}
