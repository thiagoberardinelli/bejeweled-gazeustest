using UnityEngine;
using UnityEngine.UI;

public class SFXButtonBehaviour : MonoBehaviour
{
    public Sprite DisabledSprite;

    private Sprite _enabledSprite;
    private Image _image;
    private bool _isOn = true;

    private void Awake()
    {
        _image = GetComponent<Image>();
        _enabledSprite = _image.sprite;
    }

    public void SwapSprite()
    {        
        if (_isOn)
        {            
            _image.sprite = DisabledSprite;
        }
        else
        {
            _image.sprite = _enabledSprite;
        }

        // mute all SFX sounds in the audio manager
        AudioManager.instance.MuteSoundsByType(Sound.SoundType.SFX);
        _isOn = !_isOn;
    }
}
